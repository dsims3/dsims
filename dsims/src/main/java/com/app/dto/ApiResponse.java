package com.app.dto;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
//Use this response object for sending back after validating in Spring Security filter chains
public class ApiResponse {
	private String authToken;
	boolean isLoggedIn;
	private String userName;
	private LocalDateTime stamp;

	public ApiResponse(String authToken, boolean isLoggedIn, String userName) {
		super();
		this.authToken = authToken;
		this.isLoggedIn = isLoggedIn;
		this.userName = userName;
		this.stamp = LocalDateTime.now();
	}
}