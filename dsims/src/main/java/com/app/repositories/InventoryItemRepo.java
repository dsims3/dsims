package com.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.pojos.InventoryItem;

public interface InventoryItemRepo extends JpaRepository<InventoryItem, Long> {

}
