package com.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.app.pojos.Inventory;

@Repository
public interface InventoryRepo extends JpaRepository<Inventory, Long> {

}
