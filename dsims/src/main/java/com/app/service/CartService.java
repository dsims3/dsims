package com.app.service;

import com.app.pojos.Cart;

public interface CartService {
	String addToCart(Long inventoryItemId, Long userId, int quantity);

	Cart fetchCart(Long id);

	String removeFromCart(Long cartItemId);

	String emptyCart(Long cartId);
}
