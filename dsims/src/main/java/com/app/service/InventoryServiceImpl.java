package com.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.centralexception.CustomCentralException;
import com.app.pojos.Inventory;
import com.app.pojos.InventoryItem;
import com.app.pojos.User;
import com.app.repositories.InventoryItemRepo;
import com.app.repositories.InventoryRepo;
import com.app.repositories.UserRepo;

@Service
@Transactional
public class InventoryServiceImpl implements InventoryService {
	@Autowired
	InventoryRepo inventoryRepo;

	@Autowired
	UserRepo userRepo;
	@Autowired
	InventoryItemRepo inventoryItemRepo;

	@Override
	public String addToInventory(Long id, InventoryItem inventoryItem) {
		User user = userRepo.findById(id).orElseThrow(() -> new CustomCentralException("User Id Invalid!"));
		Inventory inventory = user.getInventory();
		List<InventoryItem> list = inventory.getItems();
		list.add(inventoryItem);
		inventory.setItems(list);
		inventory.setTotalPrice(inventory.getTotalPrice() + inventoryItem.getPrice());
		inventory.setTotalQuantity(inventory.getTotalQuantity() + inventoryItem.getQuantity());

		inventoryItemRepo.save(inventoryItem);
		inventoryRepo.save(inventory);
		return "Resources credidted to Inventory with id: " + id + " and it is updated successfully!";
	}

	@Override
	public Inventory fetchInventory(Long id) {
		User user = userRepo.findById(id).orElseThrow(() -> new CustomCentralException("User Id Invalid!"));
		return user.getInventory();
	}

	@Override
	public String removeFromInventory(Long inventoryId, Long inventoryItemId) {

		Inventory inventory = inventoryRepo.findById(inventoryId)
				.orElseThrow(() -> new CustomCentralException("Invalid Inventory id, try again!!!"));

		InventoryItem inventoryItem = inventoryItemRepo.findById(inventoryItemId)
				.orElseThrow(() -> new CustomCentralException("Invalid InventoryItem id"));

		List<InventoryItem> list = inventory.getItems();
		list.remove(inventoryItem);
		inventory.setItems(list);

		inventory.setTotalPrice(inventory.getTotalPrice() - inventoryItem.getPrice());
		inventory.setTotalQuantity(inventory.getTotalQuantity() - inventoryItem.getQuantity());

		inventoryItemRepo.delete(inventoryItem);
		inventoryRepo.save(inventory);

		return "Resources deducted from Inventory with id: " + inventoryId + " and it is updated successfully!";
	}

	public void populateInventory(User user) {
		Inventory inventory = inventoryRepo.findById(user.getInventory().getId())
				.orElseThrow(() -> new CustomCentralException("Invalid Inventory id, try again!!!"));
		int totalQuantity = inventory.getItems().stream().mapToInt(invItem -> invItem.getQuantity()).sum();
		inventory.setTotalQuantity(totalQuantity);

		double totalPrice = inventory.getItems().stream().mapToDouble(invItem -> invItem.getPrice()).sum();
		inventory.setTotalPrice(totalPrice);
		inventoryRepo.save(inventory);

	}

}