package com.app.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.centralexception.CustomCentralException;
import com.app.dto.GetOrder;
import com.app.pojos.Cart;
import com.app.pojos.CartItem;
import com.app.pojos.Inventory;
import com.app.pojos.InventoryItem;
import com.app.pojos.Order;
import com.app.pojos.OrderDetails;
import com.app.pojos.Product;
import com.app.pojos.Role;
import com.app.pojos.Status;
import com.app.pojos.User;
import com.app.repositories.InventoryItemRepo;
import com.app.repositories.InventoryRepo;
import com.app.repositories.OrderDetailsRepo;
import com.app.repositories.OrderRepo;
import com.app.repositories.UserRepo;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

	@Autowired
	OrderRepo orderRepo;

	@Autowired
	InventoryRepo inventoryRepo;

	@Autowired
	InventoryService inventoryService;

	@Autowired
	InventoryItemRepo inventoryItemRepo;

	@Autowired
	UserRepo userRepo;

	@Autowired
	CartService cartService;
	@Autowired
	OrderDetailsRepo orderDetailsRepo;

	@Override
	public List<String> placeOrder(Long userId) {

		List<String> response = new ArrayList<String>();

		User user = userRepo.findById(userId).orElseThrow(() -> new CustomCentralException("User Id Invalid!"));
		Cart cart = user.getCart();

		boolean[] isEverythingOutOfStock = new boolean[cart.getCartItems().size()];
		int i = 0;

		HashSet<User> manufacturers = new HashSet<User>();
		for (CartItem cartItem : cart.getCartItems()) {
			// Finding unique manufacturers for all Products
			manufacturers.add(cartItem.getProduct().getUser());
		}
		// Rehearse
		int orderCount = 0;

		for (User to : manufacturers) {
			// Separate Order of wholesaler for each distinct manufacturer
			Order order = new Order();
			orderRepo.save(order);
			order.setFrom(user);
			order.setTo(to);

			List<CartItem> cartItemsBelongingToThisUser = cart.getCartItems().stream()
					.filter((cartitem) -> cartitem.getProduct().getUser().equals(to)).collect(Collectors.toList());

			List<OrderDetails> allOrderDetails = new ArrayList<OrderDetails>();
			int totalItems = 0;
			double totalPrice = 0.0;
			for (CartItem cartItem : cartItemsBelongingToThisUser) {
				// Getting required Product
				Product requiredProduct = cartItem.getProduct();
				// We've to first check the Inventory has enough Quantity for this product and
				// then only proceed to place order
				// TO calculate available quantity

				// 1. Get all inventory Items from this manufacturer having the required Product
				Inventory manInventory = inventoryRepo.findById(to.getInventory().getId())
						.orElseThrow(() -> new CustomCentralException("Invalid Inventory id, try again!!!"));

				List<InventoryItem> invItemsBelongingToThisManufacturerForThisProduct = manInventory.getItems().stream()
						.filter(invItem -> invItem.getProduct().equals(requiredProduct)).collect(Collectors.toList());
				int invStock = invItemsBelongingToThisManufacturerForThisProduct.stream()
						.mapToInt(invItem -> invItem.getQuantity()).sum();

				// 2. Already placed orders quantity for this particular product
				// from inventory
				List<Order> oldOrdersForThisProduct = orderRepo.findByStatusAndProduct(requiredProduct);
				List<OrderDetails> orderDetailsHavingThisProduct = new ArrayList<OrderDetails>();

				for (Order o : oldOrdersForThisProduct) {
					// we've already filtered out orderDetails having requiredProduct only
					orderDetailsHavingThisProduct.addAll(o.getOrderDetails());
				}

				int alreadyOrderedQty = orderDetailsHavingThisProduct.stream().mapToInt(od -> od.getQuantity()).sum();
				// 3. Finally getting actual available stock for sale,
				// i.e. invStock - alreadySOldButNotDispatchedStock
				int availableQty = invStock - alreadyOrderedQty;
				System.out.println(to.getUserDetails().getOrgName());
				System.out.println(invStock + " - " + alreadyOrderedQty + " = " + availableQty);
				if (availableQty == 0) {
					response.add(to.getFirstName() + " form " + to.getUserDetails().getOrgName()
							+ " does not any stock for " + requiredProduct.getName());
					// Setting true if some Ordered quantity cannot be processed
					isEverythingOutOfStock[i] = true;
					System.out.println("I'm here: if (cartItem.getQuantity() < availableQty) {");
				} else if (cartItem.getQuantity() < availableQty) {

					System.out.println("I'm here:} else if (cartItem.getQuantity() < availableQty) {");
					OrderDetails orderDetails = new OrderDetails();
					orderDetails.setQuantity(cartItem.getQuantity());
					orderDetails.setPrice(cartItem.getPrice());
					orderDetails.setProduct(cartItem.getProduct());
					orderDetails.setOrder(order);
					allOrderDetails.add(orderDetails);
					totalItems++;
					totalPrice = totalPrice + cartItem.getPrice();
					orderDetailsRepo.save(orderDetails);
					response.add("Order for " + requiredProduct.getName() + " is placed");
					System.out.println(orderDetails.toString());
				} else {
					System.out.println("I'm here: } else { stock is less");
					OrderDetails orderDetails = new OrderDetails();
					orderDetails.setQuantity(availableQty);
					orderDetails.setPrice(requiredProduct.getPrice() * availableQty);
					orderDetails.setProduct(cartItem.getProduct());
					orderDetails.setOrder(order);
					allOrderDetails.add(orderDetails);
					totalItems++;
					totalPrice = totalPrice + orderDetails.getPrice();
					orderDetailsRepo.save(orderDetails);
					response.add("Order modified for " + requiredProduct.getName() + " is placed, you'll get "
							+ orderDetails.getQuantity() + " quantity for " + orderDetails.getPrice());
					System.out.println(orderDetails.toString());
				}
				// Increasing i for populating boolean array
				i++;
			}
			List<OrderDetails> ods = order.getOrderDetails();
			ods.addAll(allOrderDetails);
			order.setOrderDetails(ods);

			order.setTotalOrderItems(totalItems);

			order.setOrderTotal(totalPrice);

			order.setStatus(Status.PLACED);

			order.setPayment(Status.UNPAID);

			// Not saving order if there are no Items in the order
			if (order.getTotalOrderItems() <= 0)
				orderRepo.delete(order);
			else
				orderRepo.save(order);
		}

		// Emptying Cart of this Wholesaler
		cartService.emptyCart(cart.getId());
		System.out.println("cartService.emptyCart(cart.getId());");

		if (OrderServiceImpl.containsOnlyTrue(isEverythingOutOfStock)) {
			response.clear();
			response.add(
					"Cannot process your order as there is no stock , sorry for inconvenience, try again later once stock is available!");
			return response;
		} else {
			response.add("Final Status : " + orderCount + " Order(s) placed successfully!");
			return response;
		}
	}

	@Override
	public String confirmOrder(Long orderId) {
		// Particular single order for **single wholesaler for **single manufacturer
		Order order = orderRepo.findById(orderId)
				.orElseThrow(() -> new CustomCentralException("Invalid Order id, try again!"));
		orderRepo.save(order);
		List<OrderDetails> orderDetails = order.getOrderDetails();
		User man = order.getTo();
		User ws = order.getFrom();

		// For each product in order detail
		for (OrderDetails od : orderDetails) {
			// Finding All InventoryItems having Product of this OrderDetail from
			// Manufacturer's inventory
			List<InventoryItem> inventoryItems = man.getInventory().getItems().stream()
					.filter((invItem) -> od.getProduct().equals(invItem.getProduct())).collect(Collectors.toList());

			// Sorting all InventoryItem according to their expire Dates
			Collections.sort(inventoryItems, (item1, item2) -> item1.getExpDate().compareTo(item2.getExpDate()));

			// For each InventoryItem in sorted list
			for (InventoryItem item : inventoryItems) {
				if (od.getQuantity() == 0)
					break;
				// Condition I : ordered qty < available InventoryItem's qty

				if (od.getQuantity() < item.getQuantity()) {

					// 1. Manufacturer
					// Updating manufacture's InventoryItem
					// Manufacture's InventoryItem updated by subtracting OrderDetails quantity from
					// this InventoryItem
					item.setQuantity(item.getQuantity() - od.getQuantity());
					item.setPrice(item.getQuantity() * item.getProduct().getPrice());

					// Populated with database
					inventoryItemRepo.save(item);

					// 2. Wholesaler
					// Creating a new InventoryItem for adding it to the Wholesaler Inventory

					// i. Create new InventoryItem and add it to the database using Repositories
					// and populated a new InventoryItem according to OrderDetails
					InventoryItem wsInvItem = new InventoryItem();
					wsInvItem.setProduct(od.getProduct());
					wsInvItem.setQuantity(od.getQuantity());
					wsInvItem.setPrice(od.getPrice());
					wsInvItem.setInventory(ws.getInventory());

					// Expiry and Mfg date can only be taken from inventoryItem hence here item is
					// used
					wsInvItem.setExpDate(item.getExpDate());
					wsInvItem.setMfgDate(item.getMfgDate());
					inventoryItemRepo.save(wsInvItem);

					// ii. Adding this InventoryItem to the List of InventoryItems of Wholesaler's
					// Inventory and adding it to the database
					Inventory wsInventory = ws.getInventory();

					List<InventoryItem> listForUpdatedWSInventoryItem = wsInventory.getItems();
					// Added this InventoryItem to an independent list
					listForUpdatedWSInventoryItem.add(wsInvItem);

					// Populated inventory->List<InventoryItem> of Wholesaler and updated

					wsInventory.setItems(listForUpdatedWSInventoryItem);

					inventoryRepo.save(wsInventory);

					// Condition II : ordered qty == available InventoryItem's qty
				} else if (od.getQuantity() == item.getQuantity()) {
					// Replaced Inventory of this InvItem from Man to WS, by doing this we're
					// Subtracting this InventoryItem as whole and adding it to the WS's inventory
					// as whole

					// i. Mapping new InventoryItem with old item
					InventoryItem wsInvItem = new InventoryItem();
					wsInvItem.setProduct(item.getProduct());
					wsInvItem.setQuantity(item.getQuantity());
					wsInvItem.setPrice(item.getPrice());
					wsInvItem.setInventory(ws.getInventory());
					wsInvItem.setExpDate(item.getExpDate());
					wsInvItem.setMfgDate(item.getMfgDate());
					// Updating it with database
					inventoryItemRepo.save(wsInvItem);
					inventoryItemRepo.delete(item);

					// i. Removing this InventoryItem from the List of InventoryItems of
					// Manufacturer's
					// Inventory and updating it with the database
					// Removed this InventoryItem from an independent list as whole
					Inventory manInventory = man.getInventory();

					List<InventoryItem> listForUpdatedManInventoryItem = manInventory.getItems();
					listForUpdatedManInventoryItem.remove(item);

					// Populated Manufacturer inventory -> List<InventoryItem> and updated
					manInventory.setItems(listForUpdatedManInventoryItem);
					inventoryRepo.save(manInventory);

					// iii. Adding this InventoryItem to the List of InventoryItems of Wholesaler's
					// Inventory and adding it to the database

					// Added this InventoryItem to an independent list
					Inventory wsInventory = ws.getInventory();

					List<InventoryItem> listForUpdatedWSInventoryItem = wsInventory.getItems();
					listForUpdatedWSInventoryItem.add(wsInvItem);
					// Populated inventory->List<InventoryItem> of Wholesaler and updated
					wsInventory.setItems(listForUpdatedWSInventoryItem);

					inventoryRepo.save(wsInventory);
				} else {

					// Condition III : ordered qty > available InventoryItem's qty
					// If controller doesn't goes into wither of above if statements, it means
					// od.qty > invItem.qty

					// Replaced Inventory of this InvItem from Man to WS, by doing this we're
					// Subtracting this InventoryItem as whole and adding it to the WS's inventory
					// as whole

					// i. Replacing item's inventory from Man to WS
					InventoryItem wsInvItem = new InventoryItem();
					wsInvItem.setProduct(item.getProduct());
					wsInvItem.setQuantity(item.getQuantity());
					wsInvItem.setPrice(item.getPrice());
					wsInvItem.setInventory(ws.getInventory());
					wsInvItem.setExpDate(item.getExpDate());
					wsInvItem.setMfgDate(item.getMfgDate());
					// Updating it with database
					inventoryItemRepo.save(wsInvItem);
					inventoryItemRepo.delete(item);

					// ii. Removing this InventoryItem from the List of InventoryItems of
					// Manufacturer's
					// Inventory and updating it with the database

					// Removed this InventoryItem from an independent list as whole
					Inventory manInventory = man.getInventory();

					List<InventoryItem> listForUpdatedManInventoryItem = manInventory.getItems();
					listForUpdatedManInventoryItem.remove(item);

					// Populated inventory->List<InventoryItem> of Manufacturer and updated
					manInventory.setItems(listForUpdatedManInventoryItem);

					inventoryRepo.save(manInventory);

					// iii. Adding this InventoryItem to the List of InventoryItems of Wholesaler's
					// Inventory and adding it to the database

					// Added this InventoryItem to an independent list
					Inventory wsInventory = ws.getInventory();

					List<InventoryItem> listForUpdatedWSInventoryItem = wsInventory.getItems();
					// Added this InventoryItem to Wholesaler's inventory as whole
					listForUpdatedWSInventoryItem.add(wsInvItem);

					// Populated inventory->List<InventoryItem> of Wholesaler and updated
					wsInventory.setItems(listForUpdatedWSInventoryItem);

					inventoryRepo.save(wsInventory);

					// Lastly, updating this OrderDetail for next iteration with updated Qty and
					// price
					od.setQuantity(od.getQuantity() - item.getQuantity());
					od.setPrice(od.getPrice() - item.getPrice());
				}

			}

		}

		// As Inventories of both parties is mapped, order's other details are set
		order.setDeliveryDate(LocalDate.now());
		order.setStatus(Status.DELIVERED);
		order.setPayment(Status.PAID);
		orderRepo.save(order);
		// As per updated InventoryItem list in Inventory POJO, Qty and Price is
		// populated using following mehtod
		inventoryService.populateInventory(ws);
		inventoryService.populateInventory(man);
		return "Order success, Inventories updated!";
	}

	public static boolean containsOnlyTrue(boolean[] arr) {

		boolean ans = true;
		for (boolean b : arr)
			ans = ans && b;
		return ans;
	}

	@Override
	public List<Order> getOrdersByIdAndDatesBetweenAndTo(GetOrder orderInfo) {
		User user = userRepo.findById(orderInfo.getUserId())
				.orElseThrow(() -> new CustomCentralException("Invalid user id!"));

		if (user.getRole() == Role.MANUFACTURER)
			return orderRepo.findByDeliveryDateBetweenAndTo(orderInfo.getStartDate(), orderInfo.getEndDate(), user);
		else
			return orderRepo.findByDeliveryDateBetweenAndFrom(orderInfo.getStartDate(), orderInfo.getEndDate(), user);
	}

	@Override
	public List<Order> getOrdersByIdAndStatus(GetOrder orderInfo) {
		User user = userRepo.findById(orderInfo.getUserId())
				.orElseThrow(() -> new CustomCentralException("Invalid user id!"));

		if (user.getRole() == Role.MANUFACTURER)
			return orderRepo.findByStatusAndTo(orderInfo.getStatus(), user);
		else
			return orderRepo.findByStatusAndFrom(orderInfo.getStatus(), user);
	}
}
