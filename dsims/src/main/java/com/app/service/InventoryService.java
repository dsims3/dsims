package com.app.service;

import com.app.pojos.Inventory;
import com.app.pojos.InventoryItem;
import com.app.pojos.User;

public interface InventoryService {

	String addToInventory(Long id, InventoryItem inventoryItem);

	Inventory fetchInventory(Long id);

	String removeFromInventory(Long inventoryId, Long inventoryItemId);
	
	void populateInventory(User user);
}
