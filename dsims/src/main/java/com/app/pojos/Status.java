package com.app.pojos;

public enum Status {
	DELIVERED, PLACED, PAID, UNPAID;
}
